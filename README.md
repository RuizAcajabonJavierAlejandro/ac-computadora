```mermaid
graph TB

  

  subgraph "Von-Neumann vs Hardvard"
  Node1[VON-NEUMANN VS HARDVARD] --> Node2[Hardvard]
  Node1 --> Node3[Von-Neumann]
  Node2 --> SubGraph1[Es una arquitectura de computadora con pistas de almacenamiento y de señal <br> físicamente separadas para las instrucciones y para los datos.]
  Node3 --> SubGraph2[ Matemático húngaro-estadounidense <br> 28 de diciembre de 1903, Budapest, Hungría--8 de febrero de 1957 Bethesda]
  SubGraph2 --> SubGraph3[Le dio su nombre a la arquitectura de von Neumann, utilizada en casi todos los computadores, <br> por su publicación del concepto]
  SubGraph3 --> SubGraph4[Virtualmente, cada computador personal, microcomputador, minicomputador <br> y supercomputador es una máquina de von Neumann.]
  SubGraph4 --> SubGraph5[Participó en el desarrollo del ENIAC que sería el primer ordenador]
  SubGraph5 --> SubGraph11[Su modelo está compuesto por 4 elementos principales]
  SubGraph11 --> SubGraph12[Procesador]
  SubGraph11 --> SubGraph13[Memoria]
  SubGraph11 --> SubGraph14[Unidades de E/S]
  SubGraph11 --> SubGraph15[Buses]
  SubGraph1 --> SubGraph6[El término proviene de la computadora Harvard Mark I basada en relés, <br> que almacenaba las instrucciones sobre cintas perforadas y los datos en interruptores electromecánicos]
  SubGraph6 --> SubGraph7[Estas primeras máquinas tenían almacenamiento de datos totalmente contenido <br> dentro la unidad central de proceso, y no proporcionaban acceso al almacenamiento de instrucciones como datos]
  SubGraph7 --> SubGraph8[En una computadora que utiliza la arquitectura Harvard, la CPU puede tanto leer <br> una instrucción como realizar un acceso a la memoria de datos al mismo tiempo ]
  SubGraph8 --> SubGraph9[En la actualidad la mayoría de los procesadores implementan dichas vías de señales <br> separadas por motivos de rendimiento, pero en realidad implementan una arquitectura Harvard modificada]
 
  
  
end
```

```mermaid
graph TB

  

  subgraph "SUPERCOMPUTADORAS EN MÉXICO"
  Node1[SUPERCOMPUTADORAS EN MÉXICO] --> Node2[Que es una supercomputadora?]
  Node2 --> SubGraph1[Es aquel tipo de ordenador que presenta capacidades de cálculo muy por encima de la media. <br> De hecho, la velocidad de estas máquinas se mide en petaflops o mil billones de operaciones por segundo.]
  SubGraph1 --> SubGraph2[ACTUALIDAD]
  SubGraph1 --> SubGraph4[HISTORIA]
  SubGraph4 --> SubGraph6[Inicia la era de la computación en 1945]
  SubGraph6 --> SubGraph7[La UNAM obtiene su primer computadora IBM 650 en 1958]
  SubGraph7 --> SubGraph8[A partir de los 80's las computadoras se comercializan más]
  SubGraph2 --> SubGraph9[La UNAM obtiene su primer supercomputadora en 1991]
  SubGraph9 --> SubGraph10[ Clúster de computadoras Hewlett-Packard, llamada Kam Balam <br> con capacidad de 7,113 teraflops, siendo la número uno en América Latina hasta noviembre de 2008]
  SubGraph10 --> SubGraph11[El IPN crea el xiuhcoat, laboratorio de computo en 2012]
  SubGraph11 --> SubGraph12[La supercomputadora Miztli de la UNAM realiza 120 proyectos de investigación al año]
  SubGraph12 --> SubGraph13[La BUAP crea un laboratorio de supercomputadoras en 2018]
  
  
  
end
```
